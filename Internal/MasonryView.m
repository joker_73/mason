#import "MasonryView.h"
#import "Item.h"
#import "UIView+MasonryIdentifier.h"


typedef NSMutableArray<NSMutableArray<Item *> *> ItemsMap;
typedef void(^ItemFoundHandler)(NSArray<Item *> * _Nonnull column, Item * _Nullable item, NSUInteger columnIndex, NSUInteger itemIndex);


static UIEdgeInsets InsetsDiff(UIEdgeInsets lhs, UIEdgeInsets rhs)
{
    return UIEdgeInsetsMake(lhs.top - rhs.top, lhs.left - rhs.left, lhs.bottom - rhs.bottom, lhs.right - rhs.right);
}


@interface MasonryView () <UIScrollViewDelegate>

@property (nonatomic) ItemsMap *itemsMap;

@property (nonatomic) NSMutableDictionary<NSString *, UINib *> *registeredViews;

@property (nonatomic, readonly) CGFloat itemSpacingWidth;
@property (nonatomic, readonly) CGFloat itemSpacingHeight;

@property (nonatomic) UIView *container;
@property (nonatomic) UIScrollView *scrollView;

@end


@implementation MasonryView

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [self baseInit];
    }
    return self;
}

- (void)baseInit
{
    _numberOfColumns = 5;
    _contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    _itemSpacing = UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0);
    _columnWidth = 100.0;
    
    _registeredViews = [NSMutableDictionary dictionary];
    
    _scrollView = [UIScrollView new];
    _scrollView.delegate = self;
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_scrollView];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[scroll]-0-|"
        options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{@"scroll": _scrollView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[scroll]-0-|"
        options:NSLayoutFormatAlignAllCenterX metrics:nil views:@{@"scroll": _scrollView}]];
    
    _container = [UIView new];
    _container.backgroundColor = [UIColor brownColor];
    _container.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    _container.translatesAutoresizingMaskIntoConstraints = YES;
    _container.autoresizesSubviews = YES;
    [_scrollView addSubview:_container];
}

#pragma mark - Life Cycle

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.itemsMap == nil) {
        [self reload];
    }
}

#pragma mark - Setters

- (void)setNumberOfColumns:(NSUInteger)numberOfColumns
{
    if (_numberOfColumns != numberOfColumns) {
        _numberOfColumns = numberOfColumns;
        
        NSMutableArray<Item *> *items = [NSMutableArray array];
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stop) {
                [item.view removeFromSuperview];
                [items addObject:item];
            }];
        }];
        
        [items sortUsingComparator:^NSComparisonResult(Item * _Nonnull item1, Item * _Nonnull item2) {
            if (item1.index < item2.index) {
                return NSOrderedAscending;
            } else if (item1.index > item2.index) {
                return NSOrderedDescending;
            } else {
                return NSOrderedSame;
            }
        }];
        
        self.itemsMap = [self instancenateItemsMap];
        [items enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            [self addItemWithIndex:item.index andHeight:CGRectGetHeight(item.frame)];
        }];
        
        [self updateVisibleArea];
    }
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(contentInsets, _contentInsets)) {
        UIEdgeInsets diffContentInsets = InsetsDiff(contentInsets, _contentInsets);
        _contentInsets = contentInsets;
        
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stop) {
                item.frame = CGRectOffset(item.frame, diffContentInsets.left, diffContentInsets.top);
                item.columnHeight += diffContentInsets.top;
            }];
        }];
        
        [self updateVisibleArea];
    }
}

- (void)setItemSpacing:(UIEdgeInsets)itemSpacing
{
    if (!UIEdgeInsetsEqualToEdgeInsets(itemSpacing, _itemSpacing)) {
        UIEdgeInsets diffItemSpacing = InsetsDiff(itemSpacing, _itemSpacing);
        _itemSpacing = itemSpacing;
        
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stop) {
                CGRect frame = item.frame;
                frame.origin.x += diffItemSpacing.left * (columnIndex + 1) + diffItemSpacing.right * columnIndex;
                frame.origin.y += diffItemSpacing.top * (itemIndex + 1) + diffItemSpacing.bottom * itemIndex;
                item.frame = frame;
                
                item.height += diffItemSpacing.top + diffItemSpacing.bottom;
                item.columnHeight += (diffItemSpacing.top + diffItemSpacing.bottom) * (itemIndex + 1);
            }];
        }];
        
        [self updateVisibleArea];
    }
}

- (void)setColumnWidth:(CGFloat)columnWidth
{
    if (_columnWidth != columnWidth) {
        CGFloat diffColumnWidth = columnWidth - _columnWidth;
        _columnWidth = columnWidth;
        
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stop) {
                CGRect frame = item.frame;
                frame.origin.x += diffColumnWidth * columnIndex;
                frame.size.width += diffColumnWidth;
                item.frame = frame;
            }];
        }];
        
        [self updateVisibleArea];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateVisibleArea];
}

#pragma mark - Public

- (void)reload
{
    id<MasonryViewDataSource> dataSource = self.dataSource;
    if (dataSource != nil) {
        self.itemsMap = [self instancenateItemsMap];
        NSArray<__kindof UIView *> *subviews = [self.container subviews];
        for (UIView *view in subviews) {
            [view removeFromSuperview];
        }
        [self loadWithDataSource:dataSource];
    }
}

- (void)registerNib:(UINib *)nib forCellReuseIdentifier:(NSString *)identifier
{
    [self.registeredViews setObject:nib forKey:identifier];
}

- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier
{
    __block UIView *result = nil;
    UINib *nib = self.registeredViews[identifier];
    if (nib != nil) {
        CGRect visibleRect = self.scrollView.frame;
        visibleRect.origin.y = self.scrollView.contentOffset.y;
        visibleRect.size.width = CGFLOAT_MAX;
        visibleRect.size.height += self.contentInsets.top + self.contentInsets.bottom;
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stopColumnIterate) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stopItemIterate) {
                UIView *view = item.view;
                if (view != nil && !CGRectIntersectsRect(visibleRect, view.frame) && [view.masonryIdentifier isEqualToString:identifier]) {
                    item.view = nil;
                    result = view;
                    [result removeFromSuperview];
                    *stopItemIterate = YES;
                    *stopColumnIterate = YES;
                }
            }];
        }];
        if (result == nil) {
            result = [nib instantiateWithOwner:nil options:nil].firstObject;
            result.masonryIdentifier = identifier;
        }
    }
    return result;
}

#pragma mark - Private

- (CGFloat)itemSpacingWidth
{
    return self.itemSpacing.left + self.itemSpacing.right;
}

- (CGFloat)itemSpacingHeight
{
    return self.itemSpacing.top + self.itemSpacing.bottom;
}

- (void)loadWithDataSource:(id<MasonryViewDataSource>)dataSource
{
    NSUInteger numberOfElements = [dataSource numberOfItemsInMasonryView:self];
    for (int index = 0; index < numberOfElements; ++index) {
        CGFloat height = [dataSource masonryView:self heightForViewWithIndex:index];
        [self addItemWithIndex:index andHeight:height];
    }
    [self updateVisibleArea];
}

// Return true if the container is changed, false otherwise
- (BOOL)updateContainer
{
    CGRect previousFrame = self.container.frame;
    
    CGRect frame = CGRectZero;
    frame.size.width = self.contentInsets.left + self.contentInsets.right + (self.columnWidth + self.itemSpacingWidth) * self.numberOfColumns;
    frame.size.height = self.contentInsets.bottom + [self maxHeight];
    self.container.frame = frame;
    self.scrollView.contentSize = frame.size;
    
    return !CGRectEqualToRect(previousFrame, frame);
}

- (void)updateVisibleArea
{
    if ([self updateContainer]) {
        [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
            [column enumerateObjectsUsingBlock:^(Item * _Nonnull item, NSUInteger itemIndex, BOOL * _Nonnull stop) {
                [item syncViewFrame];
            }];
        }];
    }
    
    id<MasonryViewDataSource> dataSource = self.dataSource;
    if (dataSource != nil) {
        [self findVisibleItemsWithHandler:^(NSArray<Item *> * _Nonnull column, Item * _Nullable item, NSUInteger columnIndex, NSUInteger itemIndex) {
            if (item.view == nil) {
                UIView *view = [dataSource masonryView:self viewForIndex:item.index];
                view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
                view.translatesAutoresizingMaskIntoConstraints = YES;
                [self.container addSubview:view];
                
                item.view = view;
                [item syncViewFrame];
            }
        }];
    }
}

- (NSUInteger)columnForInsert
{
    NSMutableArray<NSValue *> *heights = [NSMutableArray array];
    [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger idx, BOOL * _Nonnull stop) {
        [heights addObject:@(column.lastObject.columnHeight)];
    }];
    NSNumber *minHeight = [heights valueForKeyPath:@"@min.self"];
    return [heights indexOfObject:minHeight];
}

- (CGFloat)maxHeight
{
    __block CGFloat maxHeight = 0.0;
    [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger idx, BOOL * _Nonnull stop) {
        maxHeight = MAX(maxHeight, column.lastObject.columnHeight);
    }];
    return maxHeight;
}

- (void)findTopEdgeItemsWithHandler:(ItemFoundHandler)handler
{
    NSComparator comparator = ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        Item *item1 = obj1;
        Item *item2 = obj2;
        if (item1.columnHeight < item2.columnHeight) {
            return NSOrderedAscending;
        } else if (item1.columnHeight > item2.columnHeight) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    };
    
    Item *item = [Item new];
    item.columnHeight = self.scrollView.contentOffset.y;  // Top edge
    [self.itemsMap enumerateObjectsUsingBlock:^(NSMutableArray<Item *> * _Nonnull column, NSUInteger columnIndex, BOOL * _Nonnull stop) {
        if (column.count != 0) {
            NSRange range = NSMakeRange(0, column.count);
            NSUInteger index = [column indexOfObject:item inSortedRange:range options:NSBinarySearchingInsertionIndex usingComparator:comparator];
            if (index == column.count) {
                handler(column, nil, columnIndex, index);
            } else {
                handler(column, column[index], columnIndex, index);
            }
        } else {
            handler(column, nil, columnIndex, column.count);
        }
    }];
}

- (void)findVisibleItemsWithHandler:(ItemFoundHandler)handler
{
    CGFloat bottomEdge = self.scrollView.contentOffset.y + CGRectGetHeight(self.scrollView.frame);
    [self findTopEdgeItemsWithHandler:^(NSArray<Item *> * _Nonnull column, Item * _Nullable item, NSUInteger columnIndex, NSUInteger itemIndex) {
        for (NSUInteger i = itemIndex; i < column.count && column[i].columnHeightWithoutSelf <= bottomEdge; ++i) {
            handler(column, column[i], columnIndex, i);
        }
    }];
}

- (void)addItemWithIndex:(NSUInteger)index andHeight:(CGFloat)height
{
    NSUInteger column = [self columnForInsert];
    CGFloat columnHeight = self.itemsMap[column].lastObject.columnHeight;
    if (columnHeight == 0) {
        columnHeight = self.contentInsets.top;
    }
    
    CGRect frame = CGRectZero;
    frame.origin.x = self.contentInsets.left + self.itemSpacing.left + (self.columnWidth + self.itemSpacingWidth) * column;
    frame.origin.y = self.itemSpacing.top + columnHeight;
    frame.size.width = self.columnWidth;
    frame.size.height = height;
    
    CGFloat itemHeight = height + self.itemSpacingHeight;
    Item *item = [[Item alloc] initWithIndex:index frame:frame height:itemHeight columnHeight:columnHeight + itemHeight];
    [self.itemsMap[column] addObject:item];
}

- (ItemsMap *)instancenateItemsMap
{
    ItemsMap *itemsMap = [NSMutableArray array];
    for (int i = 0; i < self.numberOfColumns; ++i) {
        [itemsMap addObject:[NSMutableArray array]];
    }
    return itemsMap;
}

@end
