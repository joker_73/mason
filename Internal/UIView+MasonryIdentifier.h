#import <UIKit/UIKit.h>


@interface UIView (MasonryIdentifier) <NSObject>

@property (nonatomic) NSString *masonryIdentifier;

@end
