#import "UIView+MasonryIdentifier.h"
#import <objc/runtime.h>


@implementation UIView (MasonryIdentifier)

@dynamic masonryIdentifier;

- (NSString *)masonryIdentifier
{
    NSString *result = objc_getAssociatedObject(self, @selector(masonryIdentifier));
    return result;
}

- (void)setMasonryIdentifier:(NSString *)masonryIdentifier
{
    objc_setAssociatedObject(self, @selector(masonryIdentifier), masonryIdentifier, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
