#import <UIKit/UIKit.h>


@interface Item : NSObject

@property (nonatomic) NSUInteger index;
@property (nonatomic) CGRect frame;
@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat columnHeight;

@property (nonatomic, weak) UIView *view;

@property (nonatomic, readonly) CGFloat columnHeightWithoutSelf;

- (instancetype)initWithIndex:(NSUInteger)index frame:(CGRect)frame height:(CGFloat)height columnHeight:(CGFloat)columnHeight NS_DESIGNATED_INITIALIZER;

- (void)syncViewFrame;

@end
