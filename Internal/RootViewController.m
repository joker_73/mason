#import "RootViewController.h"
#import "MasonryView.h"
#import "MasonryViewCell.h"
#import "ImageMasonryViewCell.h"


static NSString *kCellIdentifier = @"MasonryViewCell";
static NSString *kImageCellIdentifier = @"ImageMasonryViewCell";


@interface RootViewController () <MasonryViewDataSource>

@property (nonatomic) NSArray<UIImage *> *images;

@property (nonatomic) IBOutlet MasonryView *masonryView;

@property (nonatomic) IBOutlet UISlider *numberOfColumns;

@property (nonatomic) IBOutlet UISlider *contentInsetsTop;
@property (nonatomic) IBOutlet UISlider *contentInsetsLeft;
@property (nonatomic) IBOutlet UISlider *contentInsetsBottom;
@property (nonatomic) IBOutlet UISlider *contentInsetsRight;

@property (nonatomic) IBOutlet UISlider *itemSpacingTop;
@property (nonatomic) IBOutlet UISlider *itemSpacingLeft;
@property (nonatomic) IBOutlet UISlider *itemSpacingBottom;
@property (nonatomic) IBOutlet UISlider *itemSpacingRight;

@property (nonatomic) IBOutlet UISlider *columnWidth;

@end


@implementation RootViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        [self baseInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [self baseInit];
    }
    return self;
}

- (void)baseInit
{
    _images = @[[UIImage imageNamed:@"KungFuTux"], [UIImage imageNamed:@"LunchLogo"], [UIImage imageNamed:@"Rocket"], [UIImage imageNamed:@"Tux"]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.masonryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
        toItem:self.topLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
  
    self.masonryView.dataSource = self;
    
    [self.masonryView registerNib:[UINib nibWithNibName:NSStringFromClass([MasonryViewCell class]) bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    [self.masonryView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageMasonryViewCell class]) bundle:nil] forCellReuseIdentifier:kImageCellIdentifier];
    
    self.numberOfColumns.value = self.masonryView.numberOfColumns;
    self.contentInsetsTop.value = self.masonryView.contentInsets.top;
    self.contentInsetsLeft.value = self.masonryView.contentInsets.left;
    self.contentInsetsBottom.value = self.masonryView.contentInsets.bottom;
    self.contentInsetsRight.value = self.masonryView.contentInsets.right;
    self.itemSpacingTop.value = self.masonryView.itemSpacing.top;
    self.itemSpacingLeft.value = self.masonryView.itemSpacing.left;
    self.itemSpacingBottom.value = self.masonryView.itemSpacing.bottom;
    self.itemSpacingRight.value = self.masonryView.itemSpacing.right;
    self.columnWidth.value = self.masonryView.columnWidth;
}

- (IBAction)numberOfColumnsValueChanged:(UISlider *)sender
{
    self.masonryView.numberOfColumns = sender.value;
}

- (IBAction)columnWidthValueChanged:(UISlider *)sender
{
    self.masonryView.columnWidth = sender.value;
}

- (IBAction)contentInsetsValueChanged
{
    self.masonryView.contentInsets = UIEdgeInsetsMake(self.contentInsetsTop.value, self.contentInsetsLeft.value,
        self.contentInsetsBottom.value, self.contentInsetsRight.value);
}

- (IBAction)itemSpacingValueChanged
{
    self.masonryView.itemSpacing = UIEdgeInsetsMake(self.itemSpacingTop.value, self.itemSpacingLeft.value,
        self.itemSpacingBottom.value, self.itemSpacingRight.value);
}

#pragma mark - MasonryViewDataSource

- (NSInteger)numberOfItemsInMasonryView:(MasonryView *)masonryView
{
    return 1000;
}

- (CGFloat)masonryView:(MasonryView *)masonryView heightForViewWithIndex:(NSInteger)index;
{
    return arc4random() % 500 + 22;
}

- (UIView *)masonryView:(MasonryView *)masonryView viewForIndex:(NSInteger)index;
{
    if (index % 5 == 0) {
        ImageMasonryViewCell *cell = [masonryView dequeueReusableCellWithIdentifier:kImageCellIdentifier];
        cell.imageView.image = self.images[(index / 5) % self.images.count];
        return cell;
    } else {
        MasonryViewCell *cell = [masonryView dequeueReusableCellWithIdentifier:kCellIdentifier];
        cell.label.text = @(index).stringValue;
        return cell;
    }
}

@end
