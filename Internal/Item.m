#import "Item.h"


@implementation Item

- (instancetype)initWithIndex:(NSUInteger)index frame:(CGRect)frame height:(CGFloat)height columnHeight:(CGFloat)columnHeight
{
    self = [super init];
    if (self != nil) {
        _index = index;
        _frame = frame;
        _height = height;
        _columnHeight = columnHeight;
    }
    return self;
}

- (instancetype)init
{
    return [self initWithIndex:0 frame:CGRectZero height:0.0 columnHeight:0.0];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%lu, %@, %lu>", (unsigned long)self.index, NSStringFromCGRect(self.frame), (unsigned long)self.columnHeight];
}

- (CGFloat)columnHeightWithoutSelf
{
    return self.columnHeight - self.height;
}

- (void)syncViewFrame
{
    self.view.frame = self.frame;
}

@end
