#import <UIKit/UIKit.h>


@class MasonryView;


@protocol MasonryViewDataSource <NSObject>

- (NSInteger)numberOfItemsInMasonryView:(MasonryView *)masonryView;
- (CGFloat)masonryView:(MasonryView *)masonryView heightForViewWithIndex:(NSInteger)index;
- (UIView *)masonryView:(MasonryView *)masonryView viewForIndex:(NSInteger)index;

@end


@interface MasonryView : UIView

@property (nonatomic, weak) id<MasonryViewDataSource> dataSource;

@property (nonatomic) NSUInteger numberOfColumns;
@property (nonatomic) UIEdgeInsets contentInsets;
@property (nonatomic) UIEdgeInsets itemSpacing;
@property (nonatomic) CGFloat columnWidth;

- (void)reload;

- (void)registerNib:(UINib *)nib forCellReuseIdentifier:(NSString *)identifier;
- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier;

@end
