#import <UIKit/UIKit.h>


@interface ImageMasonryViewCell : UIView

@property (nonatomic) IBOutlet UIImageView *imageView;

@end
